package com.sysomos.age_gender.ImportWiki.exception;


public class AgeGenderException extends Exception {

	private static final long serialVersionUID = -6611428769319094677L;

	public AgeGenderException(String message) {
		super(message);
	}

	public AgeGenderException(Throwable cause) {
		super(cause);
	}

	public AgeGenderException(Exception e) {
		super(e);
	}
}
