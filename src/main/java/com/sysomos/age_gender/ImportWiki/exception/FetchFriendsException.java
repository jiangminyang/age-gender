package com.sysomos.age_gender.ImportWiki.exception;


public class FetchFriendsException extends AgeGenderException {

	private static final long serialVersionUID = -6611428769319094677L;

	public FetchFriendsException(String message) {
		super(message);
	}

	public FetchFriendsException(Throwable cause) {
		super(cause);
	}

	public FetchFriendsException(Exception e) {
		super(e);
	}
}
