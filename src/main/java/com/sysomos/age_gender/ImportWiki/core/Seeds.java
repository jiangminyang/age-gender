package com.sysomos.age_gender.ImportWiki.core;

import java.util.Calendar;

public class Seeds {
	private Long id;
	private Integer age;
	private Integer gender;
	
	public Seeds(long id, String ageStr, String gender) {
		this.id = id;
		Calendar calendar = Calendar.getInstance();
		this.age = (calendar.get(Calendar.YEAR) - Integer.valueOf(ageStr.substring(0, 4)));
		if (gender.compareTo("male") == 0) {
			this.gender = 1;
		}
		else {
			this.gender = 0;
		}
	}
	
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}