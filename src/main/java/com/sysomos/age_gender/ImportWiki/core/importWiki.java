package com.sysomos.age_gender.ImportWiki.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.bson.Document;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.mongodb.client.MongoCursor;
import com.sysomos.age_gender.ImportWiki.data.FetchFollowers;
import com.sysomos.age_gender.ImportWiki.data.FetchFriends;
import com.sysomos.age_gender.ImportWiki.data.FetchSuperNodes;
import com.sysomos.age_gender.ImportWiki.data.MySQLProfileData;
import com.sysomos.age_gender.ImportWiki.data.WikiTerm;
import com.sysomos.age_gender.ImportWiki.data.mongo;
import com.sysomos.age_gender.ImportWiki.data.processingData;
import com.sysomos.age_gender.ImportWiki.data.spark;
import com.sysomos.age_gender.ImportWiki.exception.FetchFriendsException;

import scala.Tuple2;

public class importWiki {
	static final String INFO_BOX_NAME = "infobox-properties_en.nt";

	public static class Parameters {
		@Parameter(names = { "-m", "-mode" }, required = true, description = "mode 1/2")
		private Integer mode = 1;
	}

	private static void insertWikiPeopleIntoMongo(String filename) {
		File file = new File(filename);
		List<Document> docs = new ArrayList<Document>();
		int count = 0;
		try {
			Scanner sc = new Scanner(file);
			WikiTerm currentTerm = new WikiTerm(sc.nextLine());
			String path = "/root/mjiang/age_gender/data/wiki.csv";
			File wikiNameOutputfile = new File(path);
			FileWriter fw = new FileWriter(wikiNameOutputfile);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				// System.out.println(currentTerm.screen_name + " " +
				// WikiTerm.getName(line));
				if (currentTerm.screen_name != null
						&& WikiTerm.getScreenName(line).compareTo(currentTerm.screen_name) == 0) {
					currentTerm.updateBirthday(line);
					currentTerm.updateName(line);
					continue;
				}
				if (currentTerm.isValid()) {
					count++;
					docs.add(currentTerm.toDoc());
					fw.write(currentTerm.screen_name + "\n");
					currentTerm = new WikiTerm(line);
					if (docs.size() >= 100000) {
						System.out.println(count);
						mongo.db().getCollection("people").insertMany(docs);
						docs = new ArrayList<Document>();
					}
				} else {
					currentTerm = new WikiTerm(line);
				}
			}
			if (currentTerm.isValid()) {
				fw.write(currentTerm.screen_name + "\n");
				docs.add(currentTerm.toDoc());
			}
			mongo.db().getCollection("people").insertMany(docs);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void updatingGenderInfo(String verifiedFile, String wikiFile) {
		Set<String> names = new HashSet<String>();
	//	File file1 = new File("/root/mjiang/age_gender/data/wiki.csv");
		File file1 = new File("verified_in_wiki.txt");
		try {
			Scanner sc1 = new Scanner(file1);
			while (sc1.hasNextLine()) {
				String line = sc1.nextLine();
				names.add(line.split(";")[0]);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		File file2 = new File(wikiFile);
		int counter = 0;
		int counter2 = 0;
		try {
			Scanner sc2 = new Scanner(file2);
			while (sc2.hasNextLine()) {
				String line = sc2.nextLine();
				String lineName = WikiTerm.getScreenName(line);
				if (names.contains(lineName)) {
					System.out.println(++counter2 + " " + lineName);
					int maleCounter = 0;
					int femaleCounter = 0;
					String[] tokens = line.split(" ");
					for (int i = 0; i < tokens.length; i++) {
						if (tokens[i].toLowerCase().compareTo("he") == 0
								|| tokens[i].toLowerCase().compareTo("his") == 0) {
							maleCounter++;
						}
						if (tokens[i].toLowerCase().compareTo("she") == 0
								|| tokens[i].toLowerCase().compareTo("her") == 0) {
							femaleCounter++;
						}
					}
					System.out.println(maleCounter + " " + femaleCounter);
		//			maleCounter++; femaleCounter++;
					String gender = "unknown";
					if (maleCounter >  femaleCounter + 1) {
						gender = "male";
					}
					if (femaleCounter > maleCounter + 1) {
						gender = "female";
					}
					/*String gender = maleCounter > femaleCounter ? "male" : "female";
					if (maleCounter == femaleCounter) {
						gender = "unknown";
					}
					if ((maleCounter > femaleCounter && maleCounter <= 1.2 * femaleCounter) || (femaleCounter > maleCounter && femaleCounter <= 1.2 * maleCounter)) {
						gender = "unknown";
					}*/
					mongo.db().getCollection("people").updateOne(new Document("screen_name", lineName),
							new Document("$set", new Document("gender", gender)));
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	static public void main(String[] args) throws FetchFriendsException, IOException, InterruptedException, ClassNotFoundException {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		mongo.mongoInit("10.12.40.68", 27017);
		if (params.mode == 0) {
			//check verified account are in wiki or not by comparing names
			spark.sparkInit("age_gender");
			MongoCursor<Document> dbResultCursor = mongo.db().getCollection("people")
					.find(new Document("gender", new Document("$ne", "unknown"))).iterator();
			Map<String, WikiTerm> names = new HashMap<String, WikiTerm>();
			while (dbResultCursor.hasNext()) {
				Document currentDoc = dbResultCursor.next();
				if (currentDoc == null) continue;
				String name = currentDoc.getString("name");
				if (name == null) continue;
				String[] nameParts = name.split(", ");
				name = nameParts[nameParts.length - 1];
				for (int i = nameParts.length - 2; i >= 0; i--) {
					name = name + " " + nameParts[i];
				}
				if (nameParts.length >= 2) {
				//	System.out.println(name);
					names.put(name.toLowerCase(), new WikiTerm(currentDoc.getString("screen_name"), name, currentDoc.getString("date_of_birth"), currentDoc.getString("gender")));
				}
			}
			System.out.println(names.size());
			Long time = new Date().getTime();
			JavaPairRDD<Long, WikiTerm> result = MySQLProfileData.fetch(names);
			System.out.println("wiki people in twitter : " + result.count());
			System.out.println("time used = " + (new Date().getTime() - time) / 1000 + "s"); 
		} else if (params.mode == 1) {
			//read wiki info box. update name and birthday to mongodb (db: wiki, collection: people)
			insertWikiPeopleIntoMongo(INFO_BOX_NAME);
		} else if (params.mode == 2) {
			//update gender information based on wiki introduction paragraph
			updatingGenderInfo("verified_in_wiki.txt", "long-abstracts_en.nt");
		} else if (params.mode == 3) {
			//label propgation including fetch followers of seeds
			spark.sparkInit("age_gender.gender");

			Map<Long, Classes> friendsMap = getFriendsMap(params);
			System.out.println("friends table writing started at " + new Date().toString());
			String path = "/root/mjiang/age_gender/data/friends.csv";
			File file = new File(path);
			FileWriter fw = new FileWriter(file);
			for (Long id : friendsMap.keySet()) {
				fw.write(Long.toString(id) + "," + friendsMap.get(id).toStringGenderOnly() + "," + friendsMap.get(id).weight
						+ "\n");
			}
			fw.close();
			System.out.println("friends table writing finished at " + new Date().toString());
			List<Long> superNodes = new FetchSuperNodes().fetch().collect();
			System.out.println("superNodes : " + superNodes.size());
			Set<Long> idSet = new HashSet<Long>(friendsMap.keySet());
			idSet.remove(new HashSet<Long>(superNodes));
			ArrayList<Long> ids = new ArrayList<Long>();
			ids.addAll(idSet);
			System.out.println("friends size: " + ids.size());
			
			FetchFollowers.fetchIdFollowersPair(ids, "/Users/mjiang/age_gender/followers/follower_friend/");
			processingData.processing(friendsMap, "/Users/mjiang/age_gender/result");
		} else {
			//label propgation including use seed follower file created by mode3 
			spark.sparkInit("age_gender");
			Map<Long, Classes> friendsMap = getFriendsMap(params);
			processingData.processing(friendsMap, "/Users/mjiang/age_gender/result");

		}
	}

	static public Map<Long, Classes> getFriendsMap(Parameters params) throws FetchFriendsException, FileNotFoundException {
		Map<String, Long> nameToId = new HashMap<String, Long>();
		File file1 = new File("resultFile.txt");

		Scanner sc1 = new Scanner(file1);

		// get verified account age gender
		List<Long> ids = new ArrayList<Long>();
		Map<Long, Tuple2<Integer, Integer>> verified = new HashMap<Long, Tuple2<Integer, Integer>>();
		Map<Long, Tuple2<Integer, Integer>> verifiedBeforeSample = new HashMap<Long, Tuple2<Integer, Integer>>();
		
		//read the result file created by gender
		//set birthday to 0000-00-00 for now
		while (sc1.hasNextLine()) {
			String line = sc1.nextLine();
	//		System.out.println(line);
			String[] idAndGender = line.split("\\^_");

			Long id = Long.valueOf(idAndGender[0]);
			String ageStr = "0000-00-00";
			String gender = idAndGender[1];

			if (id != null) {
				Seeds seed = new Seeds(id, ageStr, gender);
				verifiedBeforeSample.put(seed.getId(), new Tuple2<Integer, Integer>(seed.getAge(), seed.getGender()));
			}
		}
		int removedCounter = 0;
		int maleCounter = 0;
		
		//filter, get random 55% of male ids
		for(Long id : verifiedBeforeSample.keySet()) {
			if (verifiedBeforeSample.get(id)._2 == 1) {
				if (Math.random() < 0.45) {
					removedCounter++;
					continue;
				}
				maleCounter++;
			}
			ids.add(id);
			verified.put(id, verifiedBeforeSample.get(id));
		}
		System.out.println(ids);
		System.out.println("total = " + ids.size());
		System.out.println("male counter = " + maleCounter);
		System.out.println("removed male counter = " + removedCounter);
		Map<Long, Collection<Long>> friends = FetchFriends.fetch(ids).asMap();
		Set<Long> friendsSet = new HashSet<Long>();
		Map<Long, Classes> friendsMap = new HashMap<Long, Classes>();
		int counter = 0;

		// get all the distinct friends of verified accounts (seeds) ignore who
		// has 5K+ friends
		for (Long id : friends.keySet()) {
			System.out.println(id + " : " + friends.get(id).size());
			if (friends.get(id).size() >= 5000)
				counter++;
			else
				friendsSet.addAll(friends.get(id));
		}
		System.out.println("counter " + counter);
		System.out.println("friends size: " + friendsSet.size());

		// put all distinct friends to count
		ids = new ArrayList<Long>(friendsSet);
		Map<Long, Long> count = new HashMap<Long, Long>();
		for (int i = 0; i < ids.size(); i++) {
			count.put(ids.get(i), 0L);
		}

		// for each verified, update its friends age gender probability
		for (Long id : friends.keySet()) {
			// System.out.println(id + " : " + friends.get(id).size());

			if (friends.get(id).size() >= 5000)
				continue;
			else {
				Collection<Long> users = friends.get(id);
				for (Long key : users)
					if (count.containsKey(key)) {
						count.put(key, count.get(key) + 1);
						if (friendsMap.containsKey(key)) {
							friendsMap.put(key,
									friendsMap.get(key).addVerifiedFollower(verified.get(id)._1, verified.get(id)._2));
						} else {
							friendsMap.put(key, new Classes(verified.get(id)._1, verified.get(id)._2));
						}
					}
			}
		}
		int total = 0;
		Set<Long> idSet = new HashSet<Long>();
		for (Long id : count.keySet()) {
			if (count.get(id) >= 100)
				idSet.add(id);
		}
		Map<Long, Classes> result = new HashMap<Long, Classes>();
		for (Long id : idSet) {
			if (friendsMap.containsKey(id)) {
				result.put(id, friendsMap.get(id));
			}
		}
		return result;
	}
}