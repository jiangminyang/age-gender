package com.sysomos.age_gender.ImportWiki.core;

import java.io.Serializable;

public class Classes implements Serializable {
	//0: 17, 1: 18-35, 2: 36-50, 3: 50, 4: Male, 5: Female
	public double[] ageGenderProbs;
	public int[] classCounter;
	public int counter = 0;
	public double weight = 1.0;	
	public Classes(int age, int gender) {
		this.counter = 1;
		this.classCounter = new int[6];
		this.ageGenderProbs = new double[6];
		for(int i = 0; i < 6; i++) {
			this.classCounter[i] = 0;
			this.ageGenderProbs[i] = 0.0;
		}
		if (age <= 17) {
			this.ageGenderProbs[0] = 1.0;
		} else if (age <= 35) {
			this.ageGenderProbs[1] = 1.0;
		} else if (age <= 50) {
			this.ageGenderProbs[2] = 1.0;
		} else {
			this.ageGenderProbs[3] = 1.0;
		}
		if (gender == 1) {
			this.ageGenderProbs[4] = 1.0;
		}
		else {
			this.ageGenderProbs[5] = 1.0;
		}
		updateClassCounter();
	}
	
	private void updateClassCounter() {
		for(int i = 0; i < 6; i++) {
			this.classCounter[i] = 0;
		}
		int maxIndex = 0;
		for(int i = 1; i < 4; i++) {
			if (this.ageGenderProbs[i] >= this.ageGenderProbs[maxIndex]) {
				maxIndex = i;
			}
		}
		this.classCounter[maxIndex] = 1;
		if (this.ageGenderProbs[4] >= this.ageGenderProbs[5]) {
			this.classCounter[4] = 1;
		}
		else {
			this.classCounter[5] = 1;
		}
	}

	public Classes(Classes cls) {
		this.ageGenderProbs = new double[6];
		this.classCounter = new int[6];
		for(int i = 0; i < 6; i++) {
			this.ageGenderProbs[i] = cls.ageGenderProbs[i] * cls.weight;
			this.classCounter[i] = cls.classCounter[i];
		}
		this.weight = cls.weight;
		this.counter = 1;
	}

	public Classes update(Classes added) {
		for(int i = 0; i < 6; i++) {
			this.ageGenderProbs[i] += added.ageGenderProbs[i];
		}
		this.weight += added.weight;
		for(int i = 0; i < 6; i++) {
			this.classCounter[i] += added.classCounter[i];
		}
		this.counter += added.counter;
		return this;
	}
	
	public Classes avg() {
		for(int i = 0; i < 6; i++) {
			this.ageGenderProbs[i] /= this.weight;
		}
		return this;
	}
	
	
	public Classes addVerifiedFollower(int age, int gender) {
		double[] ageGenderProbs = new double[6];
		for(int i = 0; i < 6; i++) {
			ageGenderProbs[i] = 0.0;
		}
		if (age <= 17) {
			ageGenderProbs[0] = 1.0;
		} else if (age <= 35) {
			ageGenderProbs[1] = 1.0;
		} else if (age <= 50) {
			ageGenderProbs[2] = 1.0;
		} else {
			ageGenderProbs[3] = 1.0;
		}
		if (gender == 1) {
			ageGenderProbs[4] = 1.0;
		}
		else {
			ageGenderProbs[5] = 1.0;
		}
		
		for(int i = 0; i < 6; i++) {
			this.ageGenderProbs[i] = (this.ageGenderProbs[i] * this.counter + ageGenderProbs[i]) / (this.counter + 1);
		}
		this.counter++;
		updateClassCounter();
		return this;
	}
	
	
	public String toString() {
		return String.format("%f,%f,%f,%f,%d,%d,%d,%d,%d,%d,%d", this.ageGenderProbs[0], this.ageGenderProbs[1], this.ageGenderProbs[2], this.ageGenderProbs[4], classCounter[0], classCounter[1], classCounter[2], classCounter[3], classCounter[4], classCounter[5], counter);
	}
	public String toStringGenderOnly() {
		return String.format("%f,%f,%d,%d,%d", this.ageGenderProbs[4], this.ageGenderProbs[5], classCounter[4], classCounter[5], counter);
	}
}