package com.sysomos.age_gender.ImportWiki.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.age_gender.ImportWiki.exception.FetchFollowersException;
import com.sysomos.core.search.transfer.Relationship;
import com.sysomos.core.search.twitter.impl.HbaseRelationshipSearchServiceImpl;

import scala.Tuple2;

public class FetchFollowers {

	private static final Logger LOG = LoggerFactory.getLogger(FetchFollowers.class);
	private static final Integer PARTITION_SIZE = 8;

	private static List<List<Long>> batches(List<Long> seedList) {
		List<List<Long>> batches = new ArrayList<List<Long>>();
		List<Long> batch = new ArrayList<Long>();
		for (Long id : seedList) {
			if (batch.size() == 20) {
				batches.add(batch);
				batch = new ArrayList<Long>();
			}
			batch.add(id);
		}
		if (batch.size() != 0) {
			batches.add(batch);
		}
		return batches;
	}
	static String path = "";
	static File file;
	static FileWriter fw;
	
	private static void delete(String a) throws IOException {
		Configuration conf = spark.getSparkContext().hadoopConfiguration();
		FileSystem fs = FileSystem.get(conf);
		Path path = new Path(a);
		fs.delete(path, true);
	}
	
	public static void fetchIdFollowersPair(List<Long> seedList, String filepath) throws IOException {
		List<List<Long>> batch = batches(seedList);
		JavaPairRDD<Long, Long> result = null;
		JavaRDD<String> batchRDD = null;
		delete(filepath);
		List<Tuple2<Long, Collection<Long>>> results;
		int index = 0;
		int step = 8;
		while (index < batch.size()) {
			batchRDD = spark.getSparkContext()
					.parallelize(batch.subList(index, Math.min(index + step, batch.size())), PARTITION_SIZE)
					.flatMapToPair(new PairFlatMapFunction<List<Long>, Long, Long>() {

						@Override
						public Iterable<Tuple2<Long, Long>> call(List<Long> batch) throws Exception {
							HbaseRelationshipSearchServiceImpl service;
							service = new HbaseRelationshipSearchServiceImpl();
							List<Tuple2<Long, Collection<Long>>> results = service.get(Relationship.FOLLOWERS, batch,
									true);
							List<Tuple2<Long, Long>> list = new ArrayList<Tuple2<Long, Long>>();
							for (int i = 0; i < results.size(); i++) {
								Collection<Long> temp = results.get(i)._2();
								for (Long key : temp) {
									list.add(new Tuple2<Long, Long>(results.get(i)._1, key));
								}
							}
							return list;
						}

					}).map(new Function<Tuple2<Long, Long>, String>() {

						@Override
						public String call(Tuple2<Long, Long> pair) throws Exception {
							return pair._1 + "," + pair._2;
						}
						
					});
			batchRDD.saveAsTextFile(filepath + "/" + index / step);
			System.out.println("batch #" + index / step + " relationship size = " + batchRDD.count());
			index += step;
		}
	}

	public static void writeToFile(List<Tuple2<Long, ArrayList<Long>>> data, int index, int total) throws IOException {
		System.out.println("start writing to file " + index + " at " + new Date().toString());

		file = new File(String.format(path, "_" + index + "_" + total));
		fw = new FileWriter(file.getAbsoluteFile(), true);
		
		if (!file.exists()) {
			file.createNewFile();
		}
		for (int i = 0; i < data.size(); i++) {
			Tuple2<Long, ArrayList<Long>> pair = data.get(i);
			for (int j = 0; j < pair._2.size(); j++) {
				fw.write(Long.toString(pair._2.get(j)) + "," + pair._1 + "\n");
			}
		}
		fw.flush();
		System.out.println("finish writing to file " + index + " at " + new Date().toString());
	}
}
