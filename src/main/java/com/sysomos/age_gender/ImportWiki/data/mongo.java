package com.sysomos.age_gender.ImportWiki.data;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class mongo {
	static private MongoClient mongoClient;
	static private MongoDatabase db;
	static public void mongoInit(String url, Integer port) {
		mongoClient = new MongoClient(url, port);
		db = mongoClient.getDatabase("wiki");
	}
	static public void useDB(String dbName) {
		db = mongoClient.getDatabase(dbName);
	}
	
	static public MongoDatabase db() {
		return db;
	}
}