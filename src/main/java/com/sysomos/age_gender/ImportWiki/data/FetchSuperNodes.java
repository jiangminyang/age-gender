package com.sysomos.age_gender.ImportWiki.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;

import com.sysomos.age_gender.ImportWiki.config.Config;

public class FetchSuperNodes {
	protected Connection connection = null;
	protected Statement statement = null;

	public JavaRDD<Long> fetch() {
		List<Long> ret = new ArrayList<Long>();
		try {
			Class.forName(Config.SQL_DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// System.exit(1);
		}
		String user = Config.SQL_USER;
		String pwd = Config.SQL_PASSWORD;
		String dbUrl = Config.SQL_TITAN_URL;

		try {
			connection = DriverManager.getConnection(dbUrl, user, pwd);

			statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM supernode_twitter");
			while (result.next()) {
				ret.add(result.getLong(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return spark.getSparkContext().parallelize(ret);
	}
}