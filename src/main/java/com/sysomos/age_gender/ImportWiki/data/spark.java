package com.sysomos.age_gender.ImportWiki.data;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;

public class spark {
	static String APP_NAME = "location.age_gender";
	static private SparkConf sparkConf;
	static private JavaSparkContext sparkContext;
	static private SQLContext sqlContext;
	
	static public void sparkInit(String name) {
		sparkConf = new SparkConf().setAppName(name);
		sparkContext = new JavaSparkContext(sparkConf);
		sparkContext.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		sparkContext.hadoopConfiguration().set("mapreduce.map.output.compress", "false");
		sparkContext.hadoopConfiguration().set("mapreduce.output.fileoutputformat.compress", "false");
		sqlContext = new SQLContext(sparkContext.sc());
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}
	
	static public JavaSparkContext getSparkContext() {
		return sparkContext;
	}
//	
//	static public HiveContext getHiveContext() {
//		return hiveContext;
//	}

	static public SQLContext getSQLContext() {
		return sqlContext;
	}
	static public void sparkClose() {
		sparkContext.close();
	}

}