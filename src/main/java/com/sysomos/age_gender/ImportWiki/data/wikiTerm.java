package com.sysomos.age_gender.ImportWiki.data;

import java.io.Serializable;

import org.bson.Document;

public class WikiTerm implements Serializable {
	public String screen_name;
	public String name;
	public String birthDate;
	public String gender;
	public WikiTerm(String screen_name, String name, String birthDate, String gender) {
		this.screen_name = screen_name;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
	}
	public WikiTerm(String line) {
		String[] tokens = line.split("> ");
		String[] words = tokens[0].split("/");
		if (words.length > 1) {
			this.screen_name = words[words.length - 1].toLowerCase();
		}
		else {
			this.screen_name = null;
		}
		this.name = null;
		this.birthDate = null;
		this.gender = null;
		updateBirthday(line);
		updateName(line);
	}
	
	public void updateBirthday(String line) {
		if (line.indexOf("/dateOfBirth>") == -1 || !line.matches(".*\"([0-9]{4})-([0-9]{2})-([0-9]{2})\".*")) return;
		String[] tokens = line.split("> ");
		if (tokens.length <= 2) return;
		String date = tokens[2].substring(1);
		this.birthDate = date.substring(0, date.indexOf("\""));
	}
	
	public void updateName(String line) {
		if (line.indexOf("property/name>") == -1) return;
		String[] tokens = line.split("> ");
		if (tokens.length <= 2) return;
		String name = tokens[2].substring(1);
		if (name.indexOf("\"") == -1) return;
		this.name = name.substring(0, name.indexOf("\""));
	}
	
	public static String getScreenName(String line) {
		String[] tokens = line.split("> ");
		String[] words = tokens[0].split("/");
		return words.length > 1 ? words[words.length - 1].toLowerCase() : "NULL";
	}
	public static boolean userful(String line) {
		if (line.indexOf("/dateOfBirth>") == -1) return false;
		return line.matches(".*\"([0-9]{4})-([0-9]{2})-([0-9]{2})\".*");
	}
	public String toString() {
		if (this.gender == null) {
			return "name: " + this.screen_name + "\ndate of birth: " + this.birthDate;
		}
		return this.screen_name + "^_" + this.name + "^_" + this.birthDate + "^_" + this.gender; 
	}
	
	public boolean isValid() {
		if (this.screen_name == null || this.birthDate == null) return false;
		return true;
	}
	
	public Document toDoc() {
		return new Document("screen_name", this.screen_name).append("name", this.name).append("date_of_birth", this.birthDate).append("gender", "unknown");
	}
}