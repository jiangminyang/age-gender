package com.sysomos.age_gender.ImportWiki.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;

import com.sysomos.age_gender.ImportWiki.config.Config;

import scala.Tuple2;

public class MySQLProfileData {
	public static List<Tuple2<Long, Tuple2<String, String>>> fetch(List<WikiTerm> data) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName(Config.SQL_DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// System.exit(1);
		}
		String user = Config.SQL_USER;
		String pwd = Config.SQL_PASSWORD;
		String dbUrl = Config.SQL_LOOKUP_TABLE_URL;
		List<Tuple2<Long, Tuple2<String, String>>> ret = new ArrayList<Tuple2<Long, Tuple2<String, String>>>();
		StringBuilder names = new StringBuilder();
		for (int i = 0; i < data.size(); i++) {
			if (i % 1000 == 0 && i != 0) {
				try {
					connection = DriverManager.getConnection(dbUrl, user, pwd);

					statement = connection.createStatement();
					String query = "select id, name, screen_name from user_profile where name in (" + names.toString()
							+ ") and followers_count >= 10000 and friends_count <= 1000";
					System.out.println(query);
					ResultSet result = statement.executeQuery(query);
					while (result.next()) {
						ret.add(new Tuple2<Long, Tuple2<String, String>>(result.getLong(1),
								new Tuple2<String, String>(result.getString(2), result.getString(3))));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				names = new StringBuilder();
				names.append("\"" + data.get(i).name + "\"");
				continue;
			}
			if (i != 0) {
				names.append(",\"" + data.get(i).name + "\"");
			} else {
				names.append("\"" + data.get(i).name + "\"");
			}
		}
		try {
			connection = DriverManager.getConnection(dbUrl, user, pwd);

			statement = connection.createStatement();
			String query = "select id, name, screen_name from user_profile where name in (" + names.toString()
					+ ") and followers_count >= 10000 and friends_count <= 1000";
			System.out.println(query);
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				ret.add(new Tuple2<Long, Tuple2<String, String>>(result.getLong(1),
						new Tuple2<String, String>(result.getString(2), result.getString(3))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static JavaPairRDD<Long, WikiTerm> fetch(Map<String, WikiTerm> data) throws ClassNotFoundException {
		final Broadcast<Map<String, WikiTerm>> verifiedBroadcast = spark.getSparkContext().broadcast(data);
		Class.forName(Config.SQL_DRIVER);
		String user = Config.SQL_USER;
		String password = Config.SQL_PASSWORD;
		String url = Config.SQL_LOOKUP_TABLE_URL;
		Map<String, String> options = new HashMap<String, String>();
		options.put("url", url + "&user=" + user + "&password=" + password);
		options.put("dbtable",
				"(select id, name, screen_name from user_profile where followers_count >= 10000 and friends_count <= 1000) as user_profile_table");
		options.put("driver", Config.SQL_DRIVER);
//		options.put("partitionColumn", "id");
//		options.put("lowerBound", "10001");
//		options.put("upperBound", "499999");
		options.put("numPartitions", "100");
		DataFrame userProfiles = spark.getSQLContext().read().format("jdbc").options(options).load();

		JavaPairRDD<Long, WikiTerm> result = userProfiles.javaRDD().repartition(60)
				.mapToPair(new PairFunction<Row, Long, Tuple2<String, String>>() {

					@Override
					public Tuple2<Long, Tuple2<String, String>> call(Row row) throws Exception {
						Long id = row.getDecimal(0).longValue();
						String name = row.getString(1);
						String screen_name = row.getString(2);
						return new Tuple2<Long, Tuple2<String, String>>(id,
								new Tuple2<String, String>(name, screen_name));

					}

				}).filter(new Function<Tuple2<Long, Tuple2<String, String>>, Boolean>() {

					@Override
					public Boolean call(Tuple2<Long, Tuple2<String, String>> row) throws Exception {
						if (!verifiedBroadcast.value().containsKey(row._2._1.toLowerCase()))
							return false;
						return true;
					}

				}).mapToPair(new PairFunction<Tuple2<Long, Tuple2<String, String>>, Long, WikiTerm>() {

					@Override
					public Tuple2<Long, WikiTerm> call(Tuple2<Long, Tuple2<String, String>> row) throws Exception {
						return new Tuple2<Long, WikiTerm>(row._1, verifiedBroadcast.value().get(row._2._1.toLowerCase()));
					}

				}).repartition(10);
		System.out.println(result.count());
		JavaRDD<String> toBeSaved = result.map(new Function<Tuple2<Long, WikiTerm>, String>() {

			@Override
			public String call(Tuple2<Long, WikiTerm> row) throws Exception {
				return row._1.toString() + "^_" + row._2.toString();
			}
		});
		toBeSaved.saveAsTextFile("/Users/mjiang/age_gender/verified_accounts");
		return result;
	}
}

// select id, screen_name, name, friends_count, followers_count from
// user_profile where name = 'John Oliver' and followers_count >= 10000 and
// friends_count <= 1000 order by (followers_count) desc limit 10;