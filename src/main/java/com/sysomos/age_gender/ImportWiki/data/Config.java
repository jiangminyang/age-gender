package com.sysomos.age_gender.ImportWiki.config;

public class Config {
	public static final String SQL_DRIVER = "com.mysql.jdbc.Driver";
	public static final String SQL_USER = "labs";
	public static final String SQL_PASSWORD = "labs01";
	public static final String SQL_LOOKUP_TABLE_URL = "jdbc:mysql://alghdn015.lab.dev.yyz.corp.pvt:3306/Lookup_tables?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false";
	public static final String SQL_TITAN_URL = "jdbc:mysql://alghdn015.lab.dev.yyz.corp.pvt:3306/titan?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false";
	public static final String AGE_GENDER_WIKI_PATH = "/Users/mjiang/age_gender/WIKI_IN_TWITTER";
}