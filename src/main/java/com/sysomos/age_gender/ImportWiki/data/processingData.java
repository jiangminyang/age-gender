package com.sysomos.age_gender.ImportWiki.data;

import java.util.Date;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import com.sysomos.age_gender.ImportWiki.core.Classes;

import scala.Tuple2;

public class processingData {
	static public void processing(Map<Long, Classes> friendsMap, String path) {
		final Broadcast<Map<Long, Classes>> broadcastSeeds = spark.getSparkContext().broadcast(friendsMap);
		JavaRDD<String> follower_friend_relationship = spark.getSparkContext().textFile("/Users/mjiang/age_gender/followers/follower_friend/");
		JavaRDD<String> result = follower_friend_relationship.mapToPair(new PairFunction<String, Long, Long>() {

			@Override
			public Tuple2<Long, Long> call(String pair) throws Exception {
				String[] ids = pair.split(",");
				return new Tuple2<Long, Long>(Long.valueOf(ids[1]), Long.valueOf(ids[0]));
			}
			
		}).mapToPair(new PairFunction<Tuple2<Long, Long>, Long, Classes>() {

			@Override
			public Tuple2<Long, Classes> call(Tuple2<Long, Long> user) throws Exception {
				return new Tuple2<Long, Classes>(user._1, new Classes(broadcastSeeds.value().get(user._2)));
			}
			
		}).reduceByKey(new Function2<Classes, Classes, Classes>() {

			@Override
			public Classes call(Classes a, Classes b) throws Exception {
				return a.update(b);
			}
			
		}).mapToPair(new PairFunction<Tuple2<Long, Classes>, Long, Classes>() {

			@Override
			public Tuple2<Long, Classes> call(Tuple2<Long, Classes> user) throws Exception {
				return new Tuple2<Long, Classes>(user._1, user._2.avg());
			}
			
		}).map(new Function<Tuple2<Long, Classes>, String>() {

			@Override
			public String call(Tuple2<Long, Classes> user) throws Exception {
				return Long.toString(user._1) + "," + user._2.toString();
			}
			
		});
	//	System.out.println(result.first());
	//	System.out.println(result.);
		saveToHDFS(result, path);
	}
	
	
	public static void saveToHDFS(JavaRDD<String> data, String path) {
		System.out.println("start writing result to HDFS at " + new Date().toString());
		data.saveAsTextFile(path);
		
		System.out.println("finish writing result to HDFS at " + new Date().toString());
	}
}